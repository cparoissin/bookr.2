########################################################
########################################################
### CHAPITRE 9 : VISUALISER UN GRAPHE ET DES RESEAUX ###
########################################################
########################################################

# Encart 1
zackary.data <- read.table(file = "./data/zachary.txt")
n <- max(zackary.data[,1:2])
label <- vector(mode = "character", length = n)
for (i in 1:n) {
  label[i] <- paste("a",i, sep = "")
}
colnames(zackary.data) <- c("from", "to", "weight")

# Encart 2
zackary.adj.matrix <- matrix(0, ncol = n, nrow = n)
for (i in 1:nrow(zackary.data)) {
  zackary.adj.matrix[zackary.data[i,1],zackary.data[i,2]] <- 1
}


#---------------------------------#
#--- PACKAGES NETWORK ET GGNET ---# 
#---------------------------------#

# Encart 1
devtools::install_github("briatte/ggnet")
library(ggnet)
library(network)
library(ggplot2)

# Encart 2
zackary.network <- network(x = zackary.adj.matrix, directed = FALSE)
network.vertex.names(zackary.network) = label

# Encart 3
ggnet2(net = zackary.network, size = 6, color = "black", edge.size = 1, edge.color = "grey")

# Encart 4
ggnet2(net = zackary.network, mode = "circle")
ggnet2(net = zackary.network, mode = "kamadakawai")

# Encart 5
ggnet2(net = zackary.network, size = "degree")
ggnet2(net = zackary.network, size = "degree", size.cut = 5)

# Encart 6
ggnet2(net = zackary.network, label = TRUE)
ggnet2(net = zackary.network, size = 12, label = TRUE, color = "black", label.color = "white")
ggnet2(net = zackary.network, color = "grey15", size = 12, label = TRUE, label.color = "red") +
  theme(panel.background = element_rect(fill = "grey15"))

#----------------------#
#--- PACKAGE GGRAPH ---# 
#----------------------#

# Encart 1
library(ggraph)
library(tibble)
library(tidyverse)
library(tidygraph)

# Encart 2
z.nodes <- tibble(id = 1:n, label = label)
z.edges <- as_tibble(zackary.data)

zackary.network <- tbl_graph(
  nodes = z.nodes, 
  edges = z.edges,
  directed = TRUE
)

# Encart 3
ggraph(zackary.network, layout = "graphopt") + 
  geom_edge_link(width = 1, colour = "lightgray") +
  geom_node_point(size = 4, colour = "#00AFBB") +
  geom_node_text(aes(label = label), repel = TRUE)+
  theme_graph()

# Encart 4
zackary.network %>%
  activate(nodes) %>%
  mutate(centrality = centrality_authority()) %>% 
  ggraph(layout = "graphopt") + 
  geom_edge_link(width = 1, colour = "lightgray") +
  geom_node_point(aes(size = centrality)) +
  geom_node_text(aes(label = label), repel = TRUE) +
  theme_graph()

# Encart 5
zackary.network %>%
  activate(nodes) %>%
  mutate(centrality = centrality_authority()) %>% 
  ggraph(layout = "graphopt") + 
  geom_edge_link(width = 1, colour = "lightgray") +
  geom_node_point(aes(colour = centrality)) +
  geom_node_text(aes(label = label), repel = TRUE) +
  scale_color_gradient(low = "yellow", high = "red") +
  theme_graph()

#------------------------------------#
#--- PACKAGES NETWORKD3 ET IGRAPH ---# 
#------------------------------------#

# Encart 1
library(networkD3)
library(igraph)
library(randomNames) 

# Encart 2
zackary.network <- graph_from_data_frame(zackary.data,  directed = FALSE, vertices = 1:n)

# Encart 3
vertices <- data.frame(
  name = V(zackary.network)$name,
  group = edge.betweenness.community(zackary.network)$membership,
  betweenness = (betweenness(zackary.network, directed = FALSE, normalized = TRUE)*115) + 0.1
) 

# Encart 4
zackary.data$source.index <- match(x = zackary.data$from, table = vertices$name) -1
zackary.data$target.index <- match(x = zackary.data$to, table = vertices$name) -1

# Encart 5
forceNetwork(Links = zackary.data, 
             Nodes = vertices,
             Source = 'source.index',
             Target = 'target.index',
             NodeID = 'name',
             Group = 'group', 
             charge = -50, 
             linkDistance = 20,
             zoom = TRUE, 
             opacity = 1,
             fontSize = 24)

# Encart 6
forceNetwork(Links = zackary.data, 
             Nodes = vertices, 
             Source = 'source.index', 
             Target = 'target.index', 
             NodeID = 'name',
             Nodesize = 'betweenness',
             Group = 'group', 
             charge = -50, # node repulsion
             linkDistance = 20,
             zoom = TRUE, 
             opacity = 1,
             fontSize=24)












