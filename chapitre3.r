#####################################################
#####################################################
### CHAPITRE 3 : NOTIONS DE BASE DE PROGRAMMATION ###
#####################################################
#####################################################

#---------------------------#
#--- OPERATEURS LOGIQUES ---# 
#---------------------------#

# Encart 1
a <- sqrt(2)
a*a==2
a*a-2
all.equal(a*a,2)

# Encart 2
# if (condition) {
#   liste1
# } else {
#   liste2
# }

# Encart 3
# ifelse(condition,instruction1,instruction2)

# Encart 4
x <- 0.3
ifelse(x<0.5,0,1)
u <- ifelse(x<0.5,0,1)
print(u)
y <- 0.7
ifelse(y<0.5,v<-0,v<-1)
print(v)

#------------------#
#--- BOUCLE FOR ---# 
#------------------#

# Encart 1
# for (i in vecteur) {
#   liste
# }

# Encart 2
n <- 3
for (i in 1:n) {
  print(i)
}

# Encart 3
n <- 5
1:n-1
1:(n-1)

#--------------------#
#--- BOUCLE WHILE ---# 
#--------------------#

# Encart 1
# while (condition) {
#   liste
# }

# Encart 2
x <- 0.75
y <- 2*x*(1-x)
while (x!=y) {
 x <- y
 y <- 2*x*(1-x)
 cat(x,y,"n")
}

#---------------------#
#--- BOUCLE REPEAT ---# 
#---------------------#

# Encart 1
# repeat {
#   liste
# }

# Encart 2
x <- 0.75
y <- 2*x*(1-x)
repeat {
 x <- y
 y <- 2*x*(1-x)
 cat(x,y,"n")
 if (x==y) {
  break 
 }
}

#-------------------------------#
#--- ACTIONS DANS UNE BOUCLE ---# 
#-------------------------------#

# Encart 1
for (i in 1:100) {
  if (i%%2==0) {
    next()
  }
  cat(i,"est un nombre impair \n")
}











