#########################################
#########################################
### CHAPITRE 2 : IMPORTER LES DONNEES ###
#########################################
#########################################

#---------------------------------------#
#--- DONNEES TABULAIRES (TEXTE, CSV) ---# 
#---------------------------------------#

# Encart 1
pierce.data <- read.table("PierceCricketData.txt")

# Encart 2
print(pierce.data)

# Encart 3
ocde.data <- read.table("OCDE.txt",header=TRUE,sep=";")
print(ocde.data)

# Encart 4
is.data.frame(ocde.data)

# Encart 5
link <- "http://pasar.blog.free.fr/public/iris.txt"
iris <- read.table(file = link, header = TRUE)

#----------------------#
#--- FICHIERS EXCEL ---# 
#----------------------#

# Encart 1
Air_Metro_Paris <- read_excel(path = "./data/Air_Metro_Paris.xls", sheet = "Auber", col_names = FALSE, skip = 10)
View(Air_Metro_Paris)

#-------------------------------------------#
#--- EXTRACTION A PARTIR D'UNE PAGE HTML ---# 
#-------------------------------------------#

# Encart 1
library(XML)
url <- "https://tinyurl.com/y6suezhn"
download.file(url, destfile = "cran.html")
X <- readHTMLTable("cran.html")
X <- X[[1]]
X <- X[nrow(X):1,]
X[,1] <- as.Date(X[,1])

Nb <- table(X[,1])
NbCum <- cumsum(Nb)
DatePubli <- unique(X[,1])

Packages <- data.frame(DatePubli,NbCum)
head(Packages)

#-------------------#
#--- FORMAT JSON ---# 
#-------------------#

# Encart 1
library(jsonlite)
sncf <- fromJSON("./data/regularite-mensuelle-intercites.json")
sncf <- sncf$fields
colnames(sncf)