##########################################
##########################################
### CHAPITRE 5 : PROGRAMMATION AVANCEE ###
##########################################
##########################################

#------------------------#
#--- CALCUL PARALLELE ---# 
#------------------------#

# Encart 1
test1 <- function(n = 100, B = 1000, alpha = 0.05) {
  beta0 <- 1
  beta1 <- .5
  s <- 1
  set.seed (123)
  x <- runif(n = n, min = 0, max = 20)
  eps <- rnorm(n = n, mean = 0, sd = 1)
  y <- beta0 + beta1 *x + s* eps
  reg <- lm(formula = y~x)
  resid <- residuals(object = reg)
  coeff <- coefficients(object = reg)
  coeff.boot <- matrix(ncol = 2, nrow = B)
  for (i in 1: B) {
    select <- sample(x = 1: n, size = n, replace = FALSE)
    yboot <- coeff [1] + coeff [2]* x + resid [select]
    coeff.boot[i ,] <- coefficients(object = lm(formula = yboot ~x))
  }
  res <- apply(X = coeff.boot, MARGIN = 2, FUN = quantile, probs = c(alpha/2,1-alpha/2))
  colnames(res) <- c("beta0", "beta1")
  return(res)
}

test1()

# Encart 2
test2 <- function(n = 100, B = 1000, alpha = 0.05) {
  beta0 <- 1
  beta1 <- .5
  s <- 1
  set.seed(123)
  x <- runif(n = n, min = 0, max = 20)
  eps <- rnorm(n = n, mean = 0, sd = 1)
  y <- beta0 + beta1 *x + s* eps
  reg <- lm(formula = y~x)
  resid <- residuals(object = reg)
  coeff <- coefficients(object = reg)
  boot.funct <- function() {
    select <- sample(x = 1: n, size = n, replace = FALSE)
    yboot <- coeff [1] + coeff [2]* x + resid [select]
    return(coefficients(object = lm(formula = yboot ~x)))
  }
  coeff.boot <- t(replicate (B, boot.funct ()))
  res <- apply(X = coeff.boot, MARGIN = 2, FUN = quantile, probs = c(alpha/2,1-alpha/2))
  colnames(res) <- c("beta0", "beta1")
  return(res)
}

test2()

# Encart 3
library(doParallel)
library(foreach)
library(doRNG)

test3 <- function(n = 100, B = 1000, alpha = 0.05) {
  NbCores <- detectCores()
  #NbCores <- 2
  cl <- makeCluster(NbCores)
  registerDoParallel(cl)
  getDoParWorkers()
  beta0 <- 1
  beta1 <- .5
  s <- 1
  set.seed(123)
  x <- runif(n = n, min = 0, max = 20) 
  eps <- rnorm(n = n, mean = 0, sd = 1)
  y <- beta0 + beta1 *x + s* eps
  reg <- lm(formula = y~x)
  resid <- residuals(object = reg)
  coeff <- coefficients(object = reg)
  boot.funct <- function() {
    select <- sample(x = 1: n, size = n, replace = FALSE)
    yboot <- coeff [1] + coeff [2]* x + resid [select]
    return(coefficients(object = lm(formula = yboot ~x)))
  }
  coeff.boot <- foreach(w = 1: B , .combine = cbind, .options.RNG =123) %dorng%
    boot.funct()
  coeff.boot = t(as.table(coeff.boot))
  res <- apply(X = coeff.boot, MARGIN = 2, FUN = quantile, probs = c(alpha/2,1-alpha/2))
  colnames(res) <- c("beta0", "beta1")
  return(res)
  stopCluster (cl)
}

test3()

# Encart 4
library(pracma)
tic() ; test1(n = 10000, B = 10000) ; toc()
tic() ; test2(n = 10000, B = 10000) ; toc()
tic() ; test3(n = 10000, B = 10000) ; toc()


#----------------------------------------------------#
#--- INTEGRER DES FONCTIONS C DANS UN PROGRAMME R ---# 
#----------------------------------------------------#

# Encart 1
dyn.load('kd.so')
is.loaded('kernel_smooth')

ksmooth.c <- function(x,xpts,h) {
  n <- length(x)
  nxpts <- length(xpts)
  dens <-.C("kernel_smooth",as.double(x),as.integer(n),
            as.double(xpts),as.integer(nxpts),
            as.double(h),result=double(length(xpts)))
  return(dens$result)
}

xpts <-(-4,4,0.01)
set.seed(100)
x <- rnorm(100)
y1 <-.c(x,xpts,0.5)

# Encart 2
ksmooth2 <- function(x,xpts,h) {
 dens <- double(length(xpts))
 n <- length(x)
 for (i in 1:length(xpts)) {
  ksum <- 0
  for (j in 1:n) {
   d <- xpts[i]-x[j]
   ksum <- ksum+dnorm(d/h)  
  }
  dens[i] <- ksum/(n*h)
 }
 return(dens)
}

# Encart 2
ksmooth3 <- function(x,xpts,h) {
 n <- length(x)
 D <- outer(x,xpts,"-")
 K <- dnorm(D/h)
 dens <- colSums(K)/(n*h)
 return(dens)
}

# Encart 3
xpts <- seq(-4,4,0.01)
set.seed(100)
x <- rnorm(100)
y1 <- ksmooth1(x,xpts,0.5)
y2 <- ksmooth2(x,xpts,0.5)
y3 <- ksmooth3(x,xpts,0.5)
all.equal(y1,y2)
all.equal(y1,y3)

#----------------------------------------------------------#
#--- INTEGRER DES FONCTIONS FORTRAN DANS UN PROGRAMME R ---# 
#----------------------------------------------------------#

# Encart 1
dyn.load("wilcox.so")
n <- 10
m <- 10
alpha <- 0.05

seuil <- qwilcox(alpha,n,m,lower.tail=FALSE)

pnmu2 <- function(n,m,r,u) {
  prb <- 0
  rec <- .Fortran("wilcox2",as.integer(n),as.integer(m),
                  as.integer(u),as.integer(r),as.double(prb))
  res <- rec[[5]][1]
  return(res)
}

k <- 3
PowerWilcox.2 <- 0
for (u in seuil:(n*m)) {
  PowerWilcox.2  <- PowerWilcox.2 + pnmu2(n,m,k,u)
}

# Encart 2
n <- 10
m <- 10
alpha <- 0.05

seuil <- qwilcox(alpha,n,m,lower.tail=FALSE)

pnmu2 <- function(n,m,u) {
  if ((n==0)|(m==0)) {
    res <- as.integer(u==0) 
  } else {
    res <- k*m*pnmu2(n,m-1,u-n)/(k*m+n) + n*pnmu2(n-1,m,u)/(k*m+n)
  }
  return(res)
}

k <- 3
PowerWilcox.2 <- 0
for (u in seuil:(n*m)) {
  PowerWilcox.2  <- PowerWilcox.2 + pnmu2(n,m,u)
}




