#################################################
#################################################
### CHAPITRE 1 : STRUCTURES DE DONNEES DANS R ###
#################################################
#################################################

#----------------#
#--- VECTEURS ---# 
#----------------#

# Encart 1
MonPremierObjet <- vector(mode = "numeric", length = 9)

# Encart 2
MonPremierObjet = vector(mode = "numeric", length = 9)
vector(mode = "numeric", length = 9) -> MonPremierObjet

# Encart 3
MonPremierObjet <- vector("numeric", 9)
#MonPremierObjet <- vector(9, "numeric") 

# Encart 4
MonPremierObjet <- vector(length = 9, mode = "numeric") 

# Encart 5
print(MonPremierObjet)

# Encart 6
x <- seq(from = -6, by = 2, to = 12)
# x <- seq(from = -16, by = -2, to = 2)

# Encart 7
x <- seq(from = -6, by = 2, length = 4)

# Encart 8
x <- -3:2

# Encart 9
Vecteur1 <- seq(from = -6, by = 2, length = 4)
rep(x = Vecteur1, times = 3)

# Encart 10
rep(x = Vecteur1, each = 3)

# Encart 11
Vecteur1 <- -1:-3
Vecteur2 <- 1:3
c(Vecteur1,Vecteur2)
c(Vecteur2,4,5,6)
c(Vecteur2,4:6)
c(Vecteur1,0,Vecteur2)

# Encart 12
index <- which((x>=2) & (x<4))
index
x[index]

# Encart 13
x <- seq(from = -6, by = 2, to = 12)
x[x<0] <- 0

# Encart 14
x <- vector(mode = "character", length = 3)
print(x)

# Encart 15
x <- c("Oui","Non","Oui","Oui","Non")

#----------------------------#
#--- MATRICES ET TABLEAUX ---#
#----------------------------#

# Encart 1
v1 <- sample(x = 0:9, size = 20, replace = TRUE)
M <- matrix(data = v1, nrow = 4, ncol = 5)

# Encart 2
matrix(data = v1, nrow = 4, ncol = 5, byrow = TRUE)

# Encart 3
#matrix (data = v1 , nrow = 4, ncol = 6)

# Encart 4
v2 <- sample(x = 0:9, size = 12, replace = TRUE)
M2 <- matrix(data = v2, nrow = 4, ncol = 3)
cbind(M,M2)

# Encart 5
v3 <- sample(x = 0:9, size = 15, replace = TRUE)
M3 <- matrix(data = v3, nrow = 3, ncol = 5)
rbind(M,M3)

# Encart 6
A <- array(data = v1, dim = c(2,2,5))

# Encart 7
A[1,2,2]
A[1,1:2,2]
A[,,2]
A[,2,]
#A[2,]
A[2,-2,2]

#--------------#
#--- LISTES ---#
#--------------#

# Encart 1
v1 <- sample(0:9, 20,TRUE)
v2 <- sample(0:9, 20,TRUE)
v3 <- sample(c(TRUE,FALSE), 20,TRUE)
v4 <- sample(c("Blue","Red"), 20,TRUE)

v <- sample(x = 0:9, size = 20, replace = TRUE)
M <- matrix(data = v, nrow = 4, ncol = 5)

L <- list(v1,v2,v3,v4,M)
print(L)

# Encart 2
L[[1]]
L[[1]][1:3]

# Encart 3
unlist(L)

#-------------------#
#--- DATA FRAMES ---#
#-------------------#

# Encart 1
DF <- data.frame(v1,v2,v3,v4)
head(DF)

# Encart 2
head(cbind(v1,v2,v3,v4))

# Encart 3
is.character(DF$v4)

# Encart 4
is.factor(DF$v4)

# Encart 5
f <- factor(v4)
print(f)

# Encart 6
levels(f)

# Encart 7
is.ordered(DF$v4)

# Encart 8
ordered(v4)

# Encart 9
is.list(DF)

# Encart 10
unlist(DF)

# Encart 11
as.matrix(DF)

# Encart 12
as.data.frame(M)

# Encart 13
unstack(DF,v1 ~ v4)

# Encart 14
split(x = DF, f = DF$v4)

# Encart 15
DF2 <- data.frame(v1,v4)
cbind.data.frame(DF,DF2)

# Encart 16
v1 <- sample(0:9, 5,TRUE)
v2 <- sample(0:9, 5,TRUE)
v3 <- sample(c(TRUE,FALSE), 5,TRUE)
v4 <- sample(c("Blue","Red"), 5,TRUE)
DF2 <- data.frame(v1,v2,v3,v4)
rbind.data.frame(DF,DF2)

# Encart 17
w4 <- sample(c("Blue","Red"), 5,TRUE)
DF2 <- data.frame(v1,v2,v3,w4)
#rbind.data.frame(DF,DF2)

# Encart 18
colnames(DF)
rownames(DF)

# Encart 19
colnames(DF) <- c("Var1", "Var2", "Var3", "Var4")
head(DF)

# Encart 20
ensemble <- c(letters,LETTERS,0:9)
Id <- replicate(n = nrow(DF), expr = paste(sample(x = ensemble, size = 5, replace = TRUE), collapse = ""))
row.names(DF) <- Id
head(DF)

# Encart 21
idx <- which(DF$Var4=="Red")
DF[idx,]

# Encart 22
subset(x = DF, subset = (Var4=="Red"))

# Encart 23
nb <- nrow(DF)
alpha <- 0.5
in.subpop <- vector(length = nb, mode = "logical")
in.subpop[sample(x = 1:nb, size = round(alpha*nb))] <- TRUE
SsPop.Train <- subset(x = DF, subset = in.subpop)
SsPop.Test <- subset(x = DF, subset = !in.subpop)

# Encart 24
#subset(x = DF, select = Var4)

# Encart 25
subset(x = DF, select = -Var4)

# Encart 26
subset(x = DF, subset = (Var4=="Red"), select = -Var4)

#---------------#
#--- TIBBLES ---#
#---------------#

# Encart 1
library(tidyverse)

v1 <- sample(0:9, 20,TRUE)
v2 <- sample(0:9, 20,TRUE)
v3 <- sample(c(TRUE,FALSE), 20,TRUE)
v4 <- sample(c("Blue","Red"), 20,TRUE)
Tbl <- tibble(v1,v2,v3,v4)

# Encart 2
as_tibble(DF)

# Encart 3
as_tibble(rownames_to_column(DF))

# Encart 4
df <- mtcars
is.data.frame(df)
print(df)

library(tidyverse)
tbl <- as_tibble(rownames_to_column(df))
print(tbl)

# Encart 5
slice(tbl, 1:5)
df[1:5, ]

# Encart 6
select(tbl, 1:4)
df[,1:3]

# Encart 7
select(tbl, -3)
df[,-3]

# Encart 8
filter(tbl, gear == 4)
df[which(df$gear == 4),]

# Encart 9
arrange(tbl, gear)
df[order(df$gear), ]

# Encart 10
arrange(tbl, desc(gear))
df[rev(order(df$gear)), ]

# Encart 11
arrange(tbl, gear, cyl)

# Encart 12
mutate(tbl, poids = wt*1000/0.4535)
df <- cbind.data.frame(df, df$wt*1000/0.4535)

# Encart 13
tbl %>% 
  filter(gear==4) %>%
  select(-c(rowname, gear)) %>%
  aggregate(.~cyl, ., mean)

# Encart 14
set.seed(123)
n <- 5

v1 <- sample(x = 0:9, size = n, replace = TRUE)
v2 <- sample(x = 0:9, size = n, replace = TRUE)
Tbl1 <- tibble(v1, v2)

v3 <- sample(x = c(TRUE,FALSE), size = n, replace = TRUE)
v4 <- sample(x = c("Blue","Red"), size = n, replace = TRUE)
Tbl2 <- tibble(v3, v4)

# Encart 15
bind_cols(Tbl1, Tbl2)

# Encart 16
set.seed(123)

n <- 5
v1 <- sample(x = 0:9, size = n, replace = TRUE)
v2 <- sample(x = 0:9, size = n, replace = TRUE)
v3 <- sample(x = c(TRUE,FALSE), size = n, replace = TRUE)
v4 <- sample(x = c("Blue","Red"), size = n, replace = TRUE)
ensemble <- c(letters, LETTERS, 0:9)
Id <- replicate(n = n, expr = paste(sample(x = ensemble, size = 5, replace = TRUE), collapse = ""))
Tbl1 <- tibble(Id, v1, v2, v3, v4)

n <- 3
v1 <- sample(x = 0:9, size = n, replace = TRUE)
v2 <- sample(x = 0:9, size = n, replace = TRUE)
v3 <- sample(x = c(TRUE,FALSE), size = n, replace = TRUE)
v4 <- sample(x = c("Blue","Red"), size = n, replace = TRUE)
ensemble <- c(letters, LETTERS, 0:9)
Id <- replicate(n = n, expr = paste(sample(x = ensemble, size = 5, replace = TRUE), collapse = ""))
Tbl2 <- tibble(Id, v1, v2, v3, v4)

# Encart 17
bind_rows(Tbl1, Tbl2)
# Encart 15

# Encart 18
Tbl2 <- tibble(Id, v1, v2, v3)
bind_rows(Tbl1, Tbl2)

# Encart 19
set.seed(123)
n <- 5
v1 <- sample(x = 0:9, size = n, replace = TRUE)
v2 <- sample(x = 0:9, size = n, replace = TRUE)
v3 <- sample(x = c(TRUE,FALSE), size = n, replace = TRUE)
v4 <- sample(x = c("Blue","Red"), size = n, replace = TRUE)
ensemble <- c(letters, LETTERS, 0:9)
Id <- replicate(n = n, expr = paste(sample(x = ensemble, size = 5, replace = TRUE), collapse = ""))

Tbl1 <- tibble(Id, v1, v2, v3)
Tbl2 <- tibble(Id, v1, v2, v4)

# Encart 20
left_join(Tbl1, Tbl2)

# Encart 21
left_join(Tbl1, Tbl2, by = "Id")

# Encart 22
idx <- sample(x = 1:n, size = n, replace = FALSE)
Tbl2 <- Tbl2[idx,]
left_join(Tbl1, Tbl2)

# Encart 23
Tbl1 <- tibble(Id, v1, v2, v3)
Tbl2 <- tibble(Id, v1, v2, v4)[-n,]
left_join(Tbl1, Tbl2)

# Encart 24
Tbl1 <- tibble(Id, v1, v2, v3)[-n,]
Tbl2 <- tibble(Id, v1, v2, v4)
left_join(Tbl1, Tbl2)

# Encart 25
Tbl1 <- tibble(Id, v1, v2)[-1,]
Tbl2 <- tibble(Id, v3, v4)[-n,]
full_join(Tbl1, Tbl2)

# Encart 26
set.seed(123)
idx1 <- sample(x = 1:n, size = ceiling(n/2), replace = FALSE)
Tbl1 <- tibble(Id, v1, v2, v3, v4)[idx1,]
idx2 <- sample(x = 1:n, size = ceiling(n/2), replace = FALSE)
Tbl2 <- tibble(Id, v1, v2, v3, v4)[idx2,]

intersect(Tbl1, Tbl2)
union(Tbl1, Tbl2)
setdiff(Tbl1, Tbl2)

#--------------------------------------#
#--- GESTION DES DONNEES MANQUANTES ---#
#--------------------------------------#

# Encart 1
v1 <- sample(0:9, 20,TRUE)
v2 <- sample(0:9, 20,TRUE)
v3 <- sample(c(TRUE,FALSE), 20,TRUE)
v4 <- sample(c("Blue","Red"), 20,TRUE)

Df <- data.frame(v1,v2,v3,v4)

ensemble <- c(letters,LETTERS,0:9)
Id <- replicate(n = nrow(Df), expr = paste(sample(x = ensemble, size = 5, replace = TRUE), collapse = ""))
row.names(Df) <- Id
head(Df)

q <- 4
idx.x <- sample(x = 1:20, size = q, replace = TRUE)
idx.y <- sample(x = 1:4, size = q, replace = TRUE)
idx <- cbind(idx.x,idx.y)
Df[idx] <- NA

# Encart 2
is.na(Df)

# Encart 3
complete.cases(Df)

# Encart 4
na.omit(Df)

# Encart 5
1 + NA

# Encart 6
mean(Df$v1)

# Encart 7
mean(Df$v1, na.rm = TRUE)

# Encart 8
Tbl <- as_tibble(Df)
drop_na(Tbl)

# Encart 9
fill(Tbl, everything())

# Encart 10
df_na_replaced <- df %>% 
  mutate_all(replace_na,0)

# Encart 11
df_na_replaced <- df %>% 
  mutate_if(is.numeric, replace_na,0)








