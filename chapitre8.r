########################################
########################################
### CHAPITRE 8 : REALISER DES CARTES ###
########################################
########################################

#-------------------------------------#
#--- LES PACKAGES MAPS ET MAPTOOLS ---# 
#-------------------------------------#

# Encart 1
Pauvrete <- read.table(file = "../Pauvrete.csv", head = TRUE, sep = "\t", dec = ",")
head(Pauvrete)

# Encart 2
library(maps)
france <- map(database = "france", plot = FALSE)
nb.id <- length(x = france$names)
id.dpt <- vector(mode = "numeric", length = nb.id)
for (i in 1:length(x = Pauvrete$Nom.dpt)) {
 idx <- grep(Pauvrete$Nom.dpt[i], france$names)
 id.dpt[idx] <- i
}

# Encart 3
library(classInt)
nbclasse <- 4
clI <- classIntervals(Pauvrete$Ensemble, n = nbclasse)
palette= clr <- rev(gray(seq(from = .4, to = .8, length = nbclasse)))
clr <- findColours(clI,palette)
clr2 <- clr[id.dpt]

# Encart 4
map(database = "france", fill = TRUE, col = clr2)
title(main = "Taux de pauvrete en 2012")
legend('bottomleft', legend = c(names(attr(clr,'table'))), fill = c(attr(clr,'palette')))

# Encart 5
fr.SPpdf <- readShapePoly("dep_france_dom")
fr.metrop <- fr.SPpdf[-(97:100),]

# Encart 6
fr.metrop <- spChFIDs(fr.metrop, row.names(Pauvrete))
fr.metrop.pauvrete <- SpatialPolygonsDataFrame(polygons(fr.metrop), Pauvrete[,-(1:2)])

# Encart 7
spplot(fr.metrop.pauvrete, zcol=colnames(Pauvrete)[-(1:3)], col.regions=rev(grey(seq(0,1,length.out=20))))

# Encart 8
routes.64<-readShapeLines("aquitaine/roads.shp", proj4string = CRS("+proj=longlat"))

# Encart 9
map('france', xlim = c(-1.8,0), ylim=c(42.8,43.7))
plot(routes.64[routes.64$type=="primary",], add = TRUE, lwd = 2, col = "lightgray")
plot(routes.64[routes.64$type == "secondary",], add = TRUE, lwd = 1, col = "lightgray")

# Encart 10
trains.64<-readShapeLines("aquitaine/railways.shp", proj4string=CRS("+proj=longlat"))

# Encart 11
plot(trains.64[trains.64$type=="rail",], add = TRUE, lwd = 1, col ="burlywood")

# Encart 12
villes.64<-readShapePoints("aquitaine/places.shp")

# Encart 13
popu.seuil <- 1000
idx <- which(!is.na(villes.64$population) & (villes.64$population>popu.seuil))
popu.pau <- villes.64$population[which((villes.64$name=="Pau") & (villes.64$type=="city"))]
symb.taille.min <- 0.2
symb.taille.max <- 2.0
pente <- (symb.taille.max-symb.taille.min)/(log(popu.pau)-log(popu.seuil))
ordo <- symb.taille.min-pente*log(popu.seuil)
symb.taille <- pente*log(villes.64$population[idx])+ordo
points(villes.64[idx,],pch=19,cex=symb.taille)

#-------------------------#
#--- LE PACKAGE RCARTO ---# 
#-------------------------#

# Encart 1
Pauvrete <- read.table(file = "Pauvrete.csv", head = TRUE, sep = "\t", dec = ",")
Pauvrete$Nom.dpt <- toupper(Pauvrete$Nom.dpt)
Pauvrete$Nom.dpt[20] <- "CORSE-DU-SUD"
Pauvrete$Nom.dpt[22] <- "COTE-D'OR"
Pauvrete$Nom.dpt[23] <- "COTES-D'ARMOR"
Pauvrete$Nom.dpt[91] <- "TERRITOIRE-DE-BELFORT"
Pauvrete$Nom.dpt[96] <- "VAL-D'OISE"
fr.SPpdf <- readShapePoly("dep_france_dom")

# Encart 2
mapChoropleth(shpFile = "dep_france_dom", shpId = "NOM_DEPT",
              df = Pauvrete, dfId = "Nom.dpt", var = "Ensemble", 
              nclass = 5, palCol = "Oranges", style = "quantile", 
               posLeg = "topleft", lgdRnd = 0, legend = "Taux de pauvrete",
              title = "Taux de pauvrete en 2012",
              author = "Champ : France metropolitaine",
              sources = "Source : Insee-DGFiP-Cnaf-Cnav-Ccmsa, Fichier localise social et fiscal")

# Encart 3
villes.aquitaine <- read.table(file = "VillesAquitaine.dat", sep = "\t", head = TRUE)

# Encart 4
mapCircles("aquitaine/places.shp", "name", villes.aquitaine, "nom", "population", author="",source="")

# Encart 5
popu.seuil <- 5000
idx <- which(villes.aquitaine$population>popu.seuil)
grandes.villes.aquitaine <- villes.aquitaine[idx,]
mapCircles("aquitaine/places.shp", "name", grandes.villes.aquitaine, "nom", "population",author="",source="")









