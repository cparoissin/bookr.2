###########################################
###########################################
### CHAPITRE 7 : REPRESENTER UNE COURBE ###
###########################################
###########################################

#-----------------------------------#
#--- TRACER UNE LIGNE POLYGONALE ---# 
#-----------------------------------#

# Encart 1
library(ggplot2)
ggplot(Packages, aes(DatePubli,NbCum)) +
  geom_line() +
  xlab("Date de publications") +
  ylab("Nombre cum. de packages")

# Encart 2
library(readr)
library(ggplot2)

DSvsS <- read_csv(file = "DS_vs_Stateux.csv", col_types = cols(Mois = col_date(format = "%Y-%m")), skip = 2)

ggplot(data = DSvsS, aes(x = Mois)) +
  xlab ("Année") +
  ylab ("Intérêt pour les recherches") + 
  geom_line(aes(y = `Statisticien: (Dans tous les pays)`, colour = "Rouge"), lwd = 1.1) +
  geom_line(aes(y = `Scientifique des données: (Dans tous les pays)`, colour = "Bleu"), lwd = 1.1) +
  scale_colour_manual("", breaks = c("Rouge", "Bleu"), values = c("red", "blue"), labels = c("Statisticien", "Scientifique des données")) 

#-----------------------------------#
#--- TRACER UNE SERIE TEMPORELLE ---# 
#-----------------------------------#

# Encart 1
library(dplyr)
library(ggplot2)

nb <- as.vector(AirPassengers)
date <- seq(from = as.Date("01/01/1949","%d/%m/%Y"), to = as.Date("01/12/1960","%d/%m/%Y"), by = "month")
air.passengers <- tibble(date,nb) 

ggplot(air.passengers, aes(x=date)) + 
  geom_area(aes(y=nb), fill = "#999999", color = "#999999", alpha=0.5) +
  theme_minimal()

# Encart 2
air.passengers <- data.frame(Year = c(floor(time(AirPassengers) + .01)), Month = c(cycle(AirPassengers)), Value = c(AirPassengers))

air.passengers$Month <- factor(air.passengers$Month)
levels(air.passengers$Month) <- month.abb

referenceLines <- air.passengers 
colnames(referenceLines)[2] <- "groupVar"
ggplot(air.passengers, aes(x = Year, y = Value)) +
  geom_line(data = referenceLines, aes(x = Year, y = Value, group = groupVar), alpha = 1/2, size = 1/2) + 
  facet_wrap(~ Month) + 
  theme_bw()








