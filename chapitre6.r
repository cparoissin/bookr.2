#####################################################
#####################################################
### CHAPITRE 6 : VISUALISER UN TABLEAU DE DONNEES ###
#####################################################
#####################################################

#--------------------------------------#
#--- REPRESENTER UN NUAGE DE POINTS ---# 
#--------------------------------------#

# Encart 1
head(Protein)
pairs(Protein[,-10])

#-----------------------------------#
#--- VISUALISER LES CORRELATIONS ---# 
#-----------------------------------#

# Encart 1
library(corrplot)
mat.cor <- cor(Protein[,-10])
corrplot(mat.cor,type="upper",order="hclust",tl.col="black",tl.srt=45)

# Encart 2
lProtein <- split(Protein[,-10],Protein[,10])
l.mat.cor <- lapply(lProtein,cor)
lapply(l.mat.cor, corrplot, type = "upper", order = "hclust", tl.col = "black", tl.srt = 45)

#----------------------------------#
#--- REPRESENTER LA VARIABILITE ---# 
#----------------------------------#

# Encart 1
library(ggplot2)
bp <- ggplot(iris, aes(x=Species, y=Sepal.Length, fill=Species)) + 
  geom_boxplot()+
  labs(title="Sepal length for each species",x="Sepcies", y = "Sepal length")
bp + theme_minimal()

#------------------------------#
#--- REPRESENTER UNE COURBE ---# 
#------------------------------#

# Encart 1
library(ggplot2)
ggplot(Packages, aes(DatePubli,NbCum)) +
  geom_line() +
  xlab("Date de publications") +
  ylab("Nombre cum. de packages")

# Encart 2
















