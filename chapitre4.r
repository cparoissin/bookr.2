#########################################
#########################################
### CHAPITRE 4 : LES FONCTIONS AVEC R ###
#########################################
#########################################

#--------------------------------#
#--- CONSTRUIRE DES FONCTIONS ---# 
#--------------------------------#

# Encart 1
f1 <- function(a, b) {
  if (is.numeric(c(a,b))) {
    if (a<b) 
      return(b)
    if (a>b) 
      return(a)
    else  
      warning("Valeurs identiques") 
  }
  else 
    stop("Au moins un des deux arguments n'est pas un flottant")
}

# Encart 2
f1(1,2)
f1(a = 1, b = 2)
f1(1,1)
f1(1,"a")

# Encart 3
f1 <- function(a, b = 1) {
  if (is.numeric(c(a,b))) {
    if (a<b) 
      return(b)
    if (a>b) 
      return(a)
    else 
      print("Valeurs identiques") 
  }
  else print("Erreur")
}

# Encart 4
f1(2)
f1(1)
f1(2,3)

#--------------------------------#
#--- SOMME, MINIMUM, ETC. ---# 
#--------------------------------#

# Encart 1
set.seed(4)
x <- sample(x = 0:9, size = 4, replace = TRUE)

# Encart 2
min(x)
max(x)
range(x)

# Encart 3
which.min(x)
which.max(x)

# Encart 4
which(x==min(x))

# Encart 5
y <- sample(x = 0:9, size = 4, replace = TRUE)
pmin(x,y)
pmax(x,y)

# Encart 6
z <- sample(x = 0:9, size = 7, replace = TRUE)
pmin(x,z)

# Encart 7
sum(x)
prod(x)

# Encart 8
cumsum(x)
cumprod(x)

# Encart 9
cummin(x)
cummax(x)

#--------------------------------#
#--- TRI, RANG ET PERMUTATION ---# 
#--------------------------------#

# Encart 1
set.seed(4)
x <- sample(x = 0:9, size = 4, replace = TRUE)
y <- sample(x = 0:9, size = 4, replace = FALSE)

# Encart 2
rev(x)

# Encart 3
sort(x)

# Encart 4
sort(x = x, decreasing = TRUE)

# Encart 5
sort(unique(x))
unique(sort(x))

# Encart 6
order(x)
x[order(x)]
sort(x)

# Encart 7
rank(y)

# Encart 8
rank(x)

#-----------------------------------------------------#
#--- COMMENT APPLIQUER UNE FONCTION A TOUT OBJET ? ---# 
#-----------------------------------------------------#

# Encart 1
x <- sample(x = 0:9, size = 20, replace = TRUE)
x <- matrix(data = x, nrow = 4, ncol = 5)
cumsum(x)
apply(X = x, MARGIN = 2, FUN = cumsum)
apply(X = x, MARGIN = 1, FUN = cumsum)
t(apply(X = x, MARGIN = 1, FUN = cumsum))

# Encart 2
v1 <- sample(x = 0:9, size = 20, replace = TRUE)
v2 <- sample(x = 0:9, size = 20, replace = TRUE)
v3 <- sample(x = c(TRUE,FALSE), size = 20, replace = TRUE)
L <- list(v1,v2,v3)
lapply(X = L, FUN = mean)
lapply(X = L, FUN = summary)

# Encart 3
pgcd <- function(a,b) {
  if (b==0) {
    return(a)
  }
  else {
    return(pgcd(b,a%%b))
  }
}
pgcd(3,9)
pgcd(a = v1, b = v2) 

# Encart 4
mapply(FUN = pgcd, a = v1, b = v2)

# Encart 5
apply(X = iris[,-5], MARGIN = 2, FUN = mean)

# Encart 6
tapply(X = iris$Sepal.Length, INDEX = iris$Species, FUN = mean)

# Encart 7
aggregate(x = iris[,-5], by = list(species=iris$Species), FUN = mean)

# Encart 8
aggregate(x = iris[,-5], by = list(species=iris$Species), FUN = summary)

# Encart 9
func <- function(x = 1, y = 2) c(x,y)
bar <- function(n, x, y) replicate(n = n, expr = func(x = x, y = y))
bar(n = 5, x = 3, y = 0)
bar(n = 4, x = runif(1), y = runif(1))

#------------------------------#
#--- ARRONDIS ET TRONCATURE ---# 
#------------------------------#

# Encart 1
print(pi)
floor(pi)
ceiling(pi)
round(pi)
round(pi,digits=2)

# Encart 2
print(pi,digits=4)
round(pi)
options(digits=6)
print(pi)
round(pi)

# Encart 3
v1 <- sample (x = 0:9 , size = 20 , replace = TRUE)
M1 <- matrix ( data = v1 , nrow = 4, ncol = 5)
v2 <- sample (x = 0:9 , size = 20 , replace = TRUE)
M2 <- matrix ( data = v2 , nrow = 5, ncol = 4)

# Encart 4
M1*M2
M1%*%M2

# Encart 5
v3 <- sample (x = 0:9 , size = 20 , replace = TRUE)
M3 <- matrix ( data = v3 , nrow = 5, ncol = 4)
M2*M3

# Encart 6
M2/M3

# Encart 7
v <- sample (x = 0:9 , size = 16 , replace = FALSE)
M <- matrix ( data = v , nrow = 4, ncol = 4)
det(M)
eigen(M)
chol(M)
IM <- solve(M)
print(IM)
M%*%IM

# Encart 8
t(M)

# Encart 9
svd(M1)








